[![gitflow-avh](https://img.shields.io/badge/gitflow--avh-used-brightgreen.svg)](https://github.com/petervanderdoes/gitflow-avh)
[![Codeship](https://img.shields.io/codeship/4b3ff270-7c43-0134-d762-0622898402f0.svg)](https://app.codeship.com/projects/181033)
[![Codacy](https://img.shields.io/codacy/grade/f4db1534ccc84e8c8b349a3e8261aab3.svg)](https://www.codacy.com/app/spilth/lionbridge-cli)

# Lionbridge Content CLI

## Prerequisites

- [Git](https://git-scm.com)
- [JDK 1.7+](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3.X](https://maven.apache.org)

### Mac

On a Mac you can install the prerequisites using [Homebrew](http://brew.sh/):

```bash
$ brew install git
$ brew cask install java
$ brew install maven
```

## Setup

To work on the project, do the following:

```bash
$ git clone git@bitbucket.org:liox-ondemand/liox-content-cli.git
$ cd liox-content-cli
$ export LIOX_API_ACCESS_KEY_ID=<your-sandbox-api-key>
$ export LIOX_API_SECRET_KEY=<your-sandbox-api-secret>
$ export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
$ export LIOX_API_DEFAULT_CURRENCY=USD
$ mvn
```

## Running

To execute the CLI, you first need to set up some environment variables:

```bash
$ export LIOX_API_ACCESS_KEY_ID=<your-sandbox-api-key>
$ export LIOX_API_SECRET_KEY=<your-sandbox-api-secret>
$ export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
$ export LIOX_API_DEFAULT_CURRENCY=USD
```

Then you can run the CLI with the following:

```bash
$ mvn package
$ alias ltrans="java -jar target/liox-content-cli.jar"
$ ltrans file add --local-file=./pom.xml --source-locale=en-us
```

## Web Site

To generate the site for the CLI:

```bash
$ mvn site
$ open target/site/index.html
```
