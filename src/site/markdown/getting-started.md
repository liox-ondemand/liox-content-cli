# Getting Started

## Register for a Sandbox Account

In order to use the Lionbridge Content CLI you'll need a Sandbox account and API keys. 

- First you need to [sign up for a sandbox account](https://developer-sandbox.liondemand.com/accounts/register/).
- Then head to [your profile page](https://developer-sandbox.liondemand.com/user/profile/).
- Click on `+ Create New Key`
- Copy the `Access Key ID` and `Access Key` values. You're gonna need these in a little bit.

## Installing the Java Development Kit

You will also need to have the [Java Development Kit 1.7 or higher](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.

Once it is installed check to make sure it is available from the command-line. You can start terminal or a command-prompt and use the `java -version` command:

```bash
→ java -version
java version "1.8.0_112"
Java(TM) SE Runtime Environment (build 1.8.0_112-b16)
Java HotSpot(TM) 64-Bit Server VM (build 25.112-b16, mixed mode)
```

## Installing the CLI

- Next you'll need to [download the latest stable version of the CLI](http://developers.lionbridge.com/content-cli/docs/download/liox-content-cli.jar).
- Then copy/move the JAR into your project's directory
- If you're using version control in your project, add the JAR there as well. This way it'll be easily available to others working on your project.

## Creating an Alias

The Lionbridge Content CLI can be made a little bit easier to use if you set up an alias to run the command.

### Mac & Linux

```bash
$ alias ltrans="java -jar liox-content-cli.jar"
```

### Windows

```commandLine
C:\> doskey ltrans=java -jar liox-content-cli.jar $*
```

## Setting Variables

In order for the Lionbridge Content CLI to securely connect to the Sandbox environment, you need to set up a few environment variables.

## Mac & Linux

```bash
$ export LIOX_API_ACCESS_KEY_ID=<your_access_key>
$ export LIOX_API_SECRET_KEY=<your_secret>
$ export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
$ export LIOX_API_DEFAULT_CURRENCY=USD
```

## Windows

```bash
c:\>setx LIOX_API_ACCESS_KEY_ID <your_access_key>
c:\>setx LIOX_API_SECRET_KEY <your_secret>
c:\>setx LIOX_API_ENDPOINT https://developer-sandbox.liondemand.com/api
c:\>setx LIOX_API_DEFAULT_CURRENCY USD
```

Now open a new command-prompt and these environment variables will be available.

## Test Command

Now it's time to make sure everything is set up correctly. First we ensure that the alias is set up correctly:

```bash
$ ltrans --version
Lionbridge Content CLI version 1.0.2
```

Then we can ensure the endpoints and API keys are set up correctly:

```bash
$ ltrans service list
services:
- name: Fashion + Apparel API
  id: 155
  validTypes:
  targetLanguages: de-de, en-gb, es-es, fr-fr, it-it
  sourceLanguages: de-de, en-gb
- name: 'Single XLIFF "XXX" translation '
  id: 156
  validTypes: xliff, zip
...etc...
```
