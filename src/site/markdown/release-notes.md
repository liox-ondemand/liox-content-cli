# Release Notes

## 1.0.3

- Adds the ability to add files by URL (reference)
- `quote authorize` can now be passed quote paid endpoints
- All commands can now take API parameters via command-line options instead of environment variables
- Fixes a bug where messages wouldn't display in non-EN_US locales.

## 1.0.2

- Uses JSDK 2.0.2
- Adds example output to each command's --help
- Speed up --help by not validating API credentials

## 1.0.1

- CLI is now built against Java 1.7 for compatibility with Content SDK for Java

## 1.0.0

- Initial release
