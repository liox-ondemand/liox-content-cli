# Overview

The Lionbridge Content Command Line Interface (LIOX CLI) allows scripts to send files for translation and retrieve translations when they are done. A common use case for the LIOX CLI is to insert translation into an automated software development lifecycle.

## Setup

The ltrans is an executable Java JAR named `liox-content-cli.jar`. To use it, you must have [Java 7+](https://www.java.com/en/download/) installed on your system and in your path.

### Download

You can download the latest JAR from the following location:

- [Latest Release Version](http://developers.lionbridge.com/content-cli/docs/download/liox-content-cli.jar)
- [Latest Development Version](http://developers.lionbridge.com/content-cli/docs/dev/download/liox-content-cli.jar)

### Alias

To make it easier to invoke from the command line, we recommend that you create an alias command like `ltrans` which expands to `java -jar liox-content-cli.jar`

On a Mac or Linux you can do this with:

```bash
alias ltrans="java -jar liox-content-cli.jar"
```

### Required API Variables

The CLI depends on a few API variables being set. This can be set either via environment variables or passed on the command-line.

#### Using Environment Variables

To use the Environment Variable method, set the following:

- `LIOX_API_ACCESS_KEY_ID`: Your Lionbridge onDemand access key ID.  You can get this on [your profile page](https://developer-sandbox.liondemand.com/user/profile/).
- `LIOX_API_SECRET_KEY`: The secret part of your Lionbridge onDemand access key. This is also available on [your profile page](https://developer-sandbox.liondemand.com/user/profile/).
- `LIOX_API_ENDPOINT`: The URL to the Lionbridge onDemand site that you are pointing to.
- `LIOX_API_DEFAULT_CURRENCY`: The default currency to use.

On a Mac or Linux you can set them as such:

```bash
export LIOX_API_ACCESS_KEY_ID=<your_access_key>
export LIOX_API_SECRET_KEY=<your_secret>
export LIOX_API_ENDPOINT=https://developer-sandbox.liondemand.com/api
export LIOX_API_DEFAULT_CURRENCY=USD
```

#### Using Command-Line Options

To use the command-line method, pass the following to any of the commands below:

- `--access-key-id`: Your Lionbridge onDemand access key ID.  You can get this on [your profile page](https://developer-sandbox.liondemand.com/user/profile/).
- `--secret-key`: The secret part of your Lionbridge onDemand access key. This is also available on [your profile page](https://developer-sandbox.liondemand.com/user/profile/).
- `--endpoint`: The URL to the Lionbridge onDemand site that you are pointing to.
- `--default-currency`: The default currency to use.

## Basic Usage

The LIOX CLI has a hierarchical command structure that looks like

```sh
$ ltrans <object-type> <command> [OPTIONS]
```

For example, the command to upload a file would look like

```sh
$ ltrans file add --local-file=./foo.txt --source-locale=en-us
```

This would return a file id that can be used in the creation of a translation job.

```sh
$ ltrans job add --source-files=999,1000 --service=10
  --source-locale=en-us --target-locales=fr-fr,it-it,es-es
```

This, in turn, can be used to create a quote.

```sh
$ ltrans quote add --jobs=500 --currency=USD
```

### Help

To get help for any command, add the `--help` argument.

### Version

To see the version of the Lionbridge Content CLI you are using, use the `--version` argument.

## Example Projects

To see how the CLI can be used in your own projects, take a look at the following:

- [Example Spring Boot Web App](https://bitbucket.org/liox-ondemand/app-example-spring)
- [Example Hot Folders App](https://bitbucket.org/liox-ondemand/app-example-hotfolders)
