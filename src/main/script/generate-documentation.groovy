import com.beust.jcommander.Parameter
import com.beust.jcommander.Parameters
import com.lionbridge.content.cli.CommandDefinition
import com.lionbridge.content.cli.ObjectServiceDefinition
import com.lionbridge.content.cli.BaseObjectService
import org.reflections.Reflections

def indexFile = new File('src/site/markdown/index.md')
def overviewFile = new File('src/site/markdown/overview.md')

def messages = ResourceBundle.getBundle("MessagesBundle")
def reflections = new Reflections("com.lionbridge.content.cli")

def objects = reflections.getSubTypesOf(BaseObjectService.class).sort { object -> object.simpleName }
def commands = reflections.getTypesAnnotatedWith(Parameters.class).sort { command -> command.simpleName }

def output = ""

output += overviewFile.getText('UTF-8')

objects.each { object ->
    def objectDefinition = object.getAnnotation(ObjectServiceDefinition.class)
    def objectName = objectDefinition.name()
    def objectDescriptionKey = objectName.toLowerCase() + ".description"

    output += "## " + objectName + " Object\n\n"

    output += messages.getString(objectDescriptionKey) + "\n\n"

    commands.each { command ->
        if (command.simpleName.startsWith(objectName)) {
            def commandDefinition = command.getAnnotation(CommandDefinition.class)

            def commandName = commandDefinition.name()
            def objectCommandKey = objectName.toLowerCase() + "." + commandName.toLowerCase()
            def commandDescriptionKey = objectCommandKey + ".description"
            def commandExampleKey = objectCommandKey + ".example"
            def commandExampleDescriptionKey = objectCommandKey + ".example-description"
            def commandOutputKey = objectCommandKey + ".output"

            def fields = command.getDeclaredFields().sort() { field -> field.name}

            output += "### " + objectName.toLowerCase() + " " + commandName.toLowerCase() + " Command\n\n"

            output += messages.getString(commandDescriptionKey) + "\n\n"

            if (fields.size() > 0) {
                output += "#### Arguments\n\n"

                fields.each { field ->
                    if (field.isAnnotationPresent(Parameter.class)) {
                        def annotation = field.getAnnotation(Parameter.class)
                        def required = annotation.required() ? "(required)" : ""

                        def fieldDescriptionKey = annotation.descriptionKey()

                        output += "* `" + annotation.names()[0] + "` " + required + ": " + messages.getString(fieldDescriptionKey) + "\n"
                    }
                }
            }

            output += "#### Output\n\n"
            output += messages.getString(commandOutputKey) + "\n\n"
            output += "#### Example Usage\n\n"
            output += messages.getString(commandExampleDescriptionKey) + "\n\n"
            output += "```sh\n" + messages.getString(commandExampleKey) + "\n```\n\n"
            output += "\n"
        }
    }

    output += "\n"
}

indexFile.write output
