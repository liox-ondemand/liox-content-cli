package com.lionbridge.content.cli;

import java.lang.reflect.InvocationTargetException;
import java.util.ResourceBundle;

import static java.lang.System.err;
import static java.lang.System.out;
import static java.text.MessageFormat.format;
import static java.util.Arrays.copyOfRange;
import static java.util.ResourceBundle.getBundle;

class Runner {
    private static ObjectServiceRegistry objectServiceRegistry = new ObjectServiceRegistry();
    private static ResourceBundle messages = getBundle("MessagesBundle");

    public void run(String... parameters) {
        if (parameters.length == 0) {
            showHelp();
        } else if (parameters.length == 1) {
            processSingleParameter(parameters[0]);
        } else {
            processMultipleParameters(parameters);
        }
    }

    private void showHelp() {
        out.printf("%s%n", messages.getString("general.usage"));
        out.printf("%s%s%n", messages.getString("general.valid-objects"), objectServiceRegistry.getObjectList());
    }

    private void showVersion() {
        out.printf("%s%n", messages.getString("general.version"));
    }

    private void processSingleParameter(String parameter) {
        if ("--help".equals(parameter)) {
            showHelp();
        } else if ("--version".equals(parameter)) {
            showVersion();
        } else {
            showObjectHelp(parameter);
        }
    }

    private void processMultipleParameters(String[] parameters) {
        if (parameters[1].equals("--help")) {
            showObjectHelp(parameters[0]);
        } else {
            String[] commandParameters = copyOfRange(parameters, 2, parameters.length);
            runObjectCommand(parameters[0], parameters[1], commandParameters);
        }
    }

    private void showObjectHelp(String object) {
        if (objectServiceRegistry.isSupportedObject(object)) {
            try {
                ObjectService objectService = objectServiceRegistry.instantiateServiceForObject(object);

                out.println(format(messages.getString("general.usage.object"), object));
                objectService.showHelp();
                out.println(format(messages.getString("general.usage.command"), object));

            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            err.println(messages.getString("general.invalid-object-type"));
            err.printf("%s%s%n", messages.getString("general.valid-objects"), objectServiceRegistry.getObjectList());
        }
    }

    private void runObjectCommand(String object, String command, String... commandParameters) {
        if (objectServiceRegistry.isSupportedObject(object)) {
            try {
                ObjectService objectService = objectServiceRegistry.instantiateServiceForObject(object);
                objectService.runCommand(command, commandParameters);
            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            err.println(messages.getString("general.invalid-object-type"));
        }
    }
}
