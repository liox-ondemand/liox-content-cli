package com.lionbridge.content.cli.quote;

import com.lionbridge.content.cli.BaseObjectService;
import com.lionbridge.content.cli.ObjectServiceDefinition;
import com.lionbridge.content.cli.utilities.ProjectService;
import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;

import static java.lang.System.err;
import static java.lang.System.out;

@ObjectServiceDefinition(name = "Quote")
public class QuoteService extends BaseObjectService {
    private ProjectService projectService = new ProjectService();
    private TranslationOptionsService translationOptionsService = new TranslationOptionsService();

    @Override
    public void runCommand(String command, String[] commandParameters) {
        if ("add".equals(command)) {
            quoteAdd(commandParameters);
        } else if ("authorize".equals(command)) {
            quoteAuthorize(commandParameters);
        } else if ("info".equals(command)) {
            quoteInfo(commandParameters);
        } else {
            err.println("Unsupported command.");
        }
    }

    @Override
    public void showHelp() {
        out.println("Valid commands are: add, authorize, info");
    }

    private void quoteAdd(String[] commandParameters) {
        QuoteAddCommand quoteAddCommand = new QuoteAddCommand();
        commander.addObject(quoteAddCommand);
        commander.parse(commandParameters);

        if (quoteAddCommand.isHelp()) {
            showObjectCommandHelp("quote.add");
        } else {
            ContentAPI contentApi = getContentApi(quoteAddCommand);

            QuoteAddExecutor quoteAddExecutor = new QuoteAddExecutor(contentApi, quoteAddCommand, translationOptionsService, projectService);
            quoteAddExecutor.execute();
        }
    }

    private void quoteAuthorize(String[] commandParameters) {
        QuoteAuthorizeCommand quoteAuthorizeCommand = new QuoteAuthorizeCommand();
        commander.addObject(quoteAuthorizeCommand);
        commander.parse(commandParameters);

        if (quoteAuthorizeCommand.isHelp()) {
            showObjectCommandHelp("quote.authorize");
        } else {
            ContentAPI contentApi = getContentApi(quoteAuthorizeCommand);

            QuoteAuthorizeExecutor quoteAuthorizeExecutor = new QuoteAuthorizeExecutor(contentApi, quoteAuthorizeCommand);
            quoteAuthorizeExecutor.execute();
        }
    }
    
    private void quoteInfo(String[] commandParameters) {
        QuoteInfoCommand quoteInfoCommand = new QuoteInfoCommand();
        commander.addObject(quoteInfoCommand);
        commander.parse(commandParameters);

        if (quoteInfoCommand.isHelp()) {
            showObjectCommandHelp("quote.info");
        } else {
            ContentAPI contentApi = getContentApi(quoteInfoCommand);

            QuoteInfoExecutor quoteInfoExecutor = new QuoteInfoExecutor(contentApi, quoteInfoCommand);
            quoteInfoExecutor.execute();
        }
    }
}
