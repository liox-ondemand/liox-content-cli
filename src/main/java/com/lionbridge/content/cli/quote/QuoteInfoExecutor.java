package com.lionbridge.content.cli.quote;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Quote;

import static java.lang.System.out;

public class QuoteInfoExecutor extends Executor {
    private final ContentAPI contentApi;
    private final QuoteInfoCommand quoteInfoCommand;

    public QuoteInfoExecutor(ContentAPI contentApi, QuoteInfoCommand quoteInfoCommand) {
        this.contentApi = contentApi;
        this.quoteInfoCommand = quoteInfoCommand;
    }

    @Override
    public void execute() {
        try {
            Quote quote = contentApi.getQuote(quoteInfoCommand.getQuoteId());
            QuotePresenter quotePresenter = new QuotePresenter(quote);

            out.println(getObjectMapper().writeValueAsString(quotePresenter));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }}
