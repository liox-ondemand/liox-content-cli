package com.lionbridge.content.cli.quote;

import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.Quote;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Collections.sort;

public class QuotePresenter {
    private Quote quote;

    public QuotePresenter(Quote quote) {
        this.quote = quote;
    }

    public String getStatus() {
        return quote.getStatus();
    }

    public String getTotalPrice() {
        return valueOf(quote.getTotalCost());
    }

    public String getCreated() {
        return valueOf(quote.getCreationDate());
    }

    public String getJobs() {
        List<String> projectIds = new ArrayList<>();

        for (Project project : quote.getProjects()) {
            projectIds.add(valueOf(project.getProjectId()));
        }

        sort(projectIds);

        return StringUtils.join(projectIds, ", ");
    }
}
