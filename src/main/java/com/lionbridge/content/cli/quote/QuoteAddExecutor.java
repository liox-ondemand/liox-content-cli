package com.lionbridge.content.cli.quote;

import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.cli.utilities.ProjectService;
import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Quote;

import static java.lang.String.valueOf;
import static java.lang.System.err;
import static java.lang.System.out;

public class QuoteAddExecutor extends Executor {
    private final ContentAPI contentApi;
    private final QuoteAddCommand quoteAddCommand;
    private final TranslationOptionsService translationOptionsService;
    private final ProjectService projectService;

    public QuoteAddExecutor(ContentAPI contentApi, QuoteAddCommand quoteAddCommand, TranslationOptionsService translationOptionsService, ProjectService projectService) {
        this.contentApi = contentApi;
        this.quoteAddCommand = quoteAddCommand;
        this.translationOptionsService = translationOptionsService;
        this.projectService = projectService;
    }

    public void execute() {
        try {
            Quote quote = contentApi.addQuote(
                    translationOptionsService.createQuoteAddTranslationOptions(quoteAddCommand),
                    projectService.createProjectsFromJobs(quoteAddCommand.getJobs()),
                    quoteAddCommand.getQuoteReadyEndPoints()
            );

            out.println(quote.getQuoteId());
        } catch (ContentAPIException e) {
            err.println(valueOf(e));
        }
    }
}
