package com.lionbridge.content.cli.quote;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdListValidator;

import java.util.List;

@CommandDefinition(name = "Add")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class QuoteAddCommand extends CliCommand {
    @Parameter(names = "--jobs", descriptionKey = "quote.add.jobs", validateWith = AssetIdListValidator.class, required = true)
    private List<String> jobs;

    @Parameter(names = "--currency", descriptionKey = "quote.add.currency" , required = true)
    private String currency;

    @Parameter(names = "--quote-ready-endpoint", descriptionKey = "quote.add.quote-ready-endpoint")
    private List<String> quoteReadyEndPoints;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public List<String> getJobs() {
        return jobs;
    }

    public String getCurrency() {
        return currency;
    }

    public boolean isHelp() {
        return help;
    }

    public List<String> getQuoteReadyEndPoints() {
        return quoteReadyEndPoints;
    }
}
