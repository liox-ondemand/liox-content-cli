package com.lionbridge.content.cli.quote;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdValidator;

@CommandDefinition(name = "Info")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class QuoteInfoCommand extends CliCommand {
    @Parameter(names = "--quote", descriptionKey = "quote.info.quote", validateWith = AssetIdValidator.class, required = true)
    private String quoteId;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getQuoteId() {
        return quoteId;
    }

    public boolean isHelp() {
        return help;
    }
}
