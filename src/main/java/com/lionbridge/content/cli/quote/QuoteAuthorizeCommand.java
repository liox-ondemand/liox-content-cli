package com.lionbridge.content.cli.quote;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdValidator;

import java.util.List;

@CommandDefinition(name = "Authorize")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class QuoteAuthorizeCommand extends CliCommand {
    @Parameter(names = "--quote", descriptionKey = "quote.authorize.quote", validateWith = AssetIdValidator.class, required = true)
    private String quote;

    @Parameter(names = "--po-number", descriptionKey = "quote.authorize.po-number", required = true)
    private String poNumber;

    @Parameter(names = "--quote-paid-endpoint", descriptionKey = "quote.authorize.quote-paid-endpoint")
    private List<String> quotePaidEndPoints;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getQuote() {
        return quote;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public List<String> getQuotePaidEndPoints() {
        return quotePaidEndPoints;
    }

    public boolean isHelp() {
        return help;
    }
}
