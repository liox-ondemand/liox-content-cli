package com.lionbridge.content.cli.quote;

import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.QuoteAuthorization;

import static java.lang.String.valueOf;
import static java.lang.System.err;
import static java.lang.System.out;

public class QuoteAuthorizeExecutor extends Executor {
    private final ContentAPI contentApi;
    private final QuoteAuthorizeCommand quoteAuthorizeCommand;

    public QuoteAuthorizeExecutor(ContentAPI contentApi, QuoteAuthorizeCommand quoteAuthorizeCommand) {
        this.contentApi = contentApi;
        this.quoteAuthorizeCommand = quoteAuthorizeCommand;
    }

    public void execute() {
        try {
            QuoteAuthorization quoteAuthorization = contentApi.authorizeQuote(
                    quoteAuthorizeCommand.getQuote(),
                    quoteAuthorizeCommand.getPoNumber(),
                    quoteAuthorizeCommand.getQuotePaidEndPoints()
            );

            if (quoteAuthorization.getStatus().equals("Authorized")) {
                out.println("Success!");
            }
        } catch (ContentAPIException e) {
            err.println(valueOf(e));
        }
    }
}
