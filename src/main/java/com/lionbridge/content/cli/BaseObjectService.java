package com.lionbridge.content.cli;

import com.beust.jcommander.JCommander;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.utilities.JCommanderBuilder;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;

import java.util.ResourceBundle;

import static java.lang.String.format;
import static java.lang.System.*;
import static java.util.ResourceBundle.getBundle;

public abstract class BaseObjectService implements ObjectService {
    protected final JCommander commander = new JCommanderBuilder().build();
    private final ResourceBundle messages = getBundle("MessagesBundle");

    private String apiAccessKeyId = getenv("LIOX_API_ACCESS_KEY_ID");
    private String apiSecretKey = getenv("LIOX_API_SECRET_KEY");
    private String apiEndpoint = getenv("LIOX_API_ENDPOINT");
    private String apiDefaultCurrency = getenv("LIOX_API_DEFAULT_CURRENCY");

    @Override
    public abstract void runCommand(String command, String[] commandParameters);

    @Override
    public abstract void showHelp();

    protected void showObjectCommandHelp(String objectCommandPrefix) {
        commander.setProgramName(messages.getString(format("%s.program-name", objectCommandPrefix)));
        commander.usage();
        out.println(messages.getString("general.example.header"));
        out.println();
        out.println(messages.getString(format("%s.example-description", objectCommandPrefix)));
        out.println();
        out.println(messages.getString(format("%s.example", objectCommandPrefix)));
    }

    protected ContentAPI getContentApi(CliCommand cliCommand) {
        ContentAPI contentApi = null;

        if (cliCommand.getApiAccessKeyId() != null) {
            apiAccessKeyId = cliCommand.getApiAccessKeyId();
        }

        if (cliCommand.getApiSecretkey() != null) {
            apiSecretKey = cliCommand.getApiSecretkey();
        }

        if (cliCommand.getApiEndPoint() != null) {
            apiEndpoint = cliCommand.getApiEndPoint();
        }

        if (cliCommand.getApiDefaultCurrency() != null) {
            apiDefaultCurrency = cliCommand.getApiDefaultCurrency();
        }

        try {
            contentApi = new ContentAPI(apiAccessKeyId, apiSecretKey, apiEndpoint, apiDefaultCurrency);

            if (!contentApi.isValid()) {
                out.println(messages.getString("general.invalid-credentials"));
                exit(1);
            }

        } catch (ContentAPIException contentApiException) {
            out.println(contentApiException.getMessage());
            exit(1);
        }

        return contentApi;
    }
}
