package com.lionbridge.content.cli.utilities;

import com.lionbridge.content.cli.job.JobAddCommand;
import com.lionbridge.content.cli.quote.QuoteAddCommand;
import com.lionbridge.content.sdk.models.SourceLanguage;
import com.lionbridge.content.sdk.models.TargetLanguage;
import com.lionbridge.content.sdk.models.TranslationOptions;

import java.util.ArrayList;
import java.util.List;

public class TranslationOptionsService {
    public TranslationOptions createJobAddTranslationOptions(JobAddCommand jobAddCommand) {
        List<TargetLanguage> targetLanguages = mapTargetLocalesToTargetLanguages(jobAddCommand);

        SourceLanguage sourceLanguage = new SourceLanguage(jobAddCommand.getSourceLocale());

        TranslationOptions translationOptions = new TranslationOptions();
        translationOptions.setServiceId(jobAddCommand.getService());
        translationOptions.setSourceLanguage(sourceLanguage);
        translationOptions.setTargetLanguages(targetLanguages);

        return translationOptions;
    }

    private List<TargetLanguage> mapTargetLocalesToTargetLanguages(JobAddCommand jobAddCommand) {
        List<TargetLanguage> targetLanguages = new ArrayList<>();

        for (String targetLocale : jobAddCommand.getTargetLocales()) {
            targetLanguages.add(new TargetLanguage(targetLocale));
        }

        return targetLanguages;
    }

    public TranslationOptions createQuoteAddTranslationOptions(QuoteAddCommand quoteAddCommand) {
        TranslationOptions translationOptions = new TranslationOptions();
        translationOptions.setCurrency(quoteAddCommand.getCurrency());

        return translationOptions;
    }
 }
