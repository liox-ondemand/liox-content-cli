package com.lionbridge.content.cli.utilities;

import com.lionbridge.content.sdk.models.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {
    public List<Project> createProjectsFromJobs(List<String> jobs) {
        List<Project> projects = new ArrayList<>();

        for (String job : jobs) {
            Project project = new Project();
            project.setProjectId(Integer.parseInt(job));
            projects.add(project);
        }
        return projects;
    }
}