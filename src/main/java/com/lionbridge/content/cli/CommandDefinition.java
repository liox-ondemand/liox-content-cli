package com.lionbridge.content.cli;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation is used by the documentation generation script to find commands and supply a name for them.
 */
@Retention(RUNTIME)
@Target(value = TYPE)
public @interface CommandDefinition {
    String name();
}
