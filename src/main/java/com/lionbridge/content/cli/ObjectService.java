package com.lionbridge.content.cli;

public interface ObjectService {
    void runCommand(String command, String[] commandParameters);
    void showHelp();
}
