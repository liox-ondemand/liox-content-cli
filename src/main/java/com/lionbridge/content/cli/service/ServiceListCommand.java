package com.lionbridge.content.cli.service;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;

@CommandDefinition(name = "List")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class ServiceListCommand extends CliCommand {
    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public boolean isHelp() {
        return help;
    }
}
