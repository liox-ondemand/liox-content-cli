package com.lionbridge.content.cli.service;

import com.lionbridge.content.sdk.models.Service;

import java.util.ArrayList;
import java.util.List;

public class ServicesPresenter {
    private List<ServicePresenter> services = new ArrayList<>();

    public ServicesPresenter(List<Service> services) {
        mapServicestoServicePresenters(services);
    }

    private void mapServicestoServicePresenters(List<Service> services) {
        for (Service service : services) {
            this.services.add(new ServicePresenter(service));
        }
    }

    public List<ServicePresenter> getServices() {
        return this.services;
    }
}
