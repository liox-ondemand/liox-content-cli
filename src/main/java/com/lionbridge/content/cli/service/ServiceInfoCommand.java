package com.lionbridge.content.cli.service;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdValidator;

@CommandDefinition(name = "Info")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class ServiceInfoCommand extends CliCommand {
    @Parameter(names = "--service", descriptionKey = "service.info.id", validateWith = AssetIdValidator.class, required = true)
    private String serviceId;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public boolean isHelp() {
        return help;
    }

    public String getServiceId() {
        return serviceId;
    }
}
