package com.lionbridge.content.cli.service;

import com.lionbridge.content.sdk.models.Service;
import com.lionbridge.content.sdk.models.SourceLanguage;
import com.lionbridge.content.sdk.models.TargetLanguage;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.sort;

public class ServicePresenter {
    private Service service;

    public ServicePresenter(Service service) {
        this.service = service;
    }

    public String getName() {
        return service.getName();
    }

    public int getId() {
        return service.getServiceId();
    }

    public String getValidTypes() {
        return sortAndJoinValidTypes();
    }

    public String getSourceLanguages() {
        return mapsSortAndJoinSourceLanguages();
    }

    public String getTargetLanguages() {
        return mapSortAndJoinTargetLanguages();
    }

    private String sortAndJoinValidTypes() {
        List<String> files = service.getValidInputs().getFiles();

        sort(files);

        return StringUtils.join(files, ", ");
    }

    private String mapSortAndJoinTargetLanguages() {
        List<String> languageCodes = new ArrayList<>();

        for (TargetLanguage targetLanguage : service.getTargetLanguages()) {
            languageCodes.add(targetLanguage.getLanguageCode());
        }

        sort(languageCodes);

        return StringUtils.join(languageCodes, ", ");
    }

    private String mapsSortAndJoinSourceLanguages() {
        List<String> languageCodes = new ArrayList<>();

        for (SourceLanguage sourceLanguage : service.getSourceLanguages()) {
            languageCodes.add(sourceLanguage.getLanguageCode());
        }

        sort(languageCodes);

        return StringUtils.join(languageCodes, ", ");
    }
}
