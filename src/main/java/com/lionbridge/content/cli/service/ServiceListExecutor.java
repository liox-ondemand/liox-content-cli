package com.lionbridge.content.cli.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Service;

import java.util.List;

import static java.lang.System.out;

public class ServiceListExecutor extends Executor {
    private final ContentAPI contentApi;

    public ServiceListExecutor(ContentAPI contentApi) {
        this.contentApi = contentApi;
    }

    public void execute() {
        try {
            List<Service> services = contentApi.getServices();
            ServicesPresenter servicesPresenter = new ServicesPresenter(services);

            out.println(getObjectMapper().writeValueAsString(servicesPresenter));

        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
