package com.lionbridge.content.cli.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Service;

import static java.lang.Integer.valueOf;
import static java.lang.System.out;

public class ServiceInfoExecutor extends Executor {
    private final ContentAPI contentApi;
    private final ServiceInfoCommand serviceInfoCommand;

    public ServiceInfoExecutor(ContentAPI contentApi, ServiceInfoCommand serviceInfoCommand) {
        this.contentApi = contentApi;
        this.serviceInfoCommand = serviceInfoCommand;
    }

    public void execute() {
        try {
            Service service = contentApi.getService(valueOf(serviceInfoCommand.getServiceId()));
            ServicePresenter servicePresenter = new ServicePresenter(service);

            out.println(getObjectMapper().writeValueAsString(servicePresenter));

        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
