package com.lionbridge.content.cli.service;

import com.lionbridge.content.cli.BaseObjectService;
import com.lionbridge.content.cli.ObjectServiceDefinition;
import com.lionbridge.content.sdk.ContentAPI;

import static java.lang.System.err;
import static java.lang.System.out;

@ObjectServiceDefinition(name = "Service")
public class ServiceService extends BaseObjectService {
    @Override
    public void runCommand(String command, String[] commandParameters) {
        if ("list".equals(command)) {
            serviceList(commandParameters);
        } else if ("info".equals(command)) {
            serviceInfo(commandParameters);
        } else {
            err.println("Unsupported command.");
        }
    }

    @Override
    public void showHelp() {
        out.println("Valid commands are: list, info");
    }

    private void serviceList(String[] commandParameters) {
        ServiceListCommand serviceListCommand = new ServiceListCommand();
        commander.addObject(serviceListCommand);
        commander.parse(commandParameters);

        if (serviceListCommand.isHelp()) {
            showObjectCommandHelp("service.list");
        } else {
            ContentAPI contentApi = getContentApi(serviceListCommand);

            ServiceListExecutor serviceListExecutor = new ServiceListExecutor(contentApi);
            serviceListExecutor.execute();
        }
    }

    private void serviceInfo(String[] commandParameters) {
        ServiceInfoCommand serviceInfoCommand = new ServiceInfoCommand();
        commander.addObject(serviceInfoCommand);
        commander.parse(commandParameters);

        if (serviceInfoCommand.isHelp()) {
            showObjectCommandHelp("service.info");
        } else {
            ContentAPI contentApi = getContentApi(serviceInfoCommand);

            ServiceInfoExecutor serviceInfoExecutor = new ServiceInfoExecutor(contentApi, serviceInfoCommand);
            serviceInfoExecutor.execute();
        }
    }
}
