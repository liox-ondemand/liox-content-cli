package com.lionbridge.content.cli.locale;

import com.lionbridge.content.sdk.models.Locale;

import java.util.ArrayList;
import java.util.List;

public class LocalesPresenter {
    private final List<LocalePresenter> locales;

    public LocalesPresenter(List<Locale> locales) {
        this.locales = mapLocalesToLocalePresenters(locales);
    }

    public List<LocalePresenter> getLocales() {
        return this.locales;
    }

    private List<LocalePresenter> mapLocalesToLocalePresenters(List<Locale> locales) {
        List<LocalePresenter> localePresenters = new ArrayList<>();

        for (Locale locale : locales) {
            localePresenters.add(new LocalePresenter(locale));
        }

        return localePresenters;
    }
}
