package com.lionbridge.content.cli.locale;

import com.lionbridge.content.cli.BaseObjectService;
import com.lionbridge.content.cli.ObjectServiceDefinition;
import com.lionbridge.content.sdk.ContentAPI;

import static java.lang.System.err;
import static java.lang.System.out;

@ObjectServiceDefinition(name = "Locale")
public class LocaleService  extends BaseObjectService {
    @Override
    public void runCommand(String command, String[] commandParameters) {
        if ("list".equals(command)) {
            localeList(commandParameters);
        } else {
            err.println("Unsupported command.");
        }
    }

    @Override
    public void showHelp() {
        out.println("Valid commands are: list");
    }

    private void localeList(String[] commandParameters) {
        LocaleListCommand localeListComand = new LocaleListCommand();
        commander.addObject(localeListComand);
        commander.parse(commandParameters);

        if (localeListComand.isHelp()) {
            showObjectCommandHelp("locale.list");
        } else {
            ContentAPI contentApi = getContentApi(localeListComand);

            LocaleListExecutor localeListExecutor = new LocaleListExecutor(contentApi);
            localeListExecutor.execute();
        }
    }
}
