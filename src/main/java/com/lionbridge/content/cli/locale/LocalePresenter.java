package com.lionbridge.content.cli.locale;

import com.lionbridge.content.sdk.models.Locale;

public class LocalePresenter {
    private final Locale locale;

    public LocalePresenter(Locale locale) {
        this.locale = locale;
    }

    public String getCode() {
        return locale.getCode();
    }

    public String getName() {
        return locale.getName();
    }

}
