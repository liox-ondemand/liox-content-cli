package com.lionbridge.content.cli.locale;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Locale;

import java.util.List;

import static java.lang.System.out;

public class LocaleListExecutor extends Executor {
    private final ContentAPI contentApi;

    public LocaleListExecutor(ContentAPI contentApi) {
        this.contentApi = contentApi;
    }

    public void execute() {
        try {
            List<Locale> locales = contentApi.getLocales();
            LocalesPresenter localesPresenter = new LocalesPresenter(locales);

            out.println(getObjectMapper().writeValueAsString(localesPresenter));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
