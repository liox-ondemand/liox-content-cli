package com.lionbridge.content.cli;

import org.apache.commons.lang3.StringUtils;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.sort;

public class ObjectServiceRegistry {
    private static final Map<String, Class<? extends ObjectService>> objectServicesMap = new HashMap<>();
    private static Reflections reflections = new Reflections("com.lionbridge.content.cli");
    private String objectList;

    public ObjectServiceRegistry() {
        findAndRegisterObjectServices();
    }

    public String getObjectList() {
        return objectList;
    }

    public boolean isSupportedObject(String objectName) {
        return objectServicesMap.keySet().contains(objectName);
    }

    public ObjectService instantiateServiceForObject(String objectName) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return objectServicesMap.get(objectName).getDeclaredConstructor().newInstance();
    }

    private void findAndRegisterObjectServices() {
        for (Class<? extends ObjectService> objectService : reflections.getSubTypesOf(BaseObjectService.class)) {
            ObjectServiceDefinition objectServiceDefinition = objectService.getAnnotation(ObjectServiceDefinition.class);
            objectServicesMap.put(objectServiceDefinition.name().toLowerCase(), objectService);
        }

        objectList = sortAndJoinObjectServicesKeys();
    }

    private String sortAndJoinObjectServicesKeys() {
        List<String> keyList = new ArrayList<>();

        for (String key : objectServicesMap.keySet()) {
            keyList.add(key);
        }

        sort(keyList);

        return StringUtils.join(keyList, ", ");
    }

}
