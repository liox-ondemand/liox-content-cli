package com.lionbridge.content.cli.job;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdValidator;

@CommandDefinition(name = "Info")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class JobInfoCommand extends CliCommand {
    @Parameter(names = "--job", descriptionKey = "job.info.job", validateWith = AssetIdValidator.class, required = true)
    private String jobId;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getJobId() {
        return jobId;
    }

    public boolean isHelp() {
        return help;
    }
}
