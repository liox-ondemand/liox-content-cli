package com.lionbridge.content.cli.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Project;

import static java.lang.System.out;

public class JobInfoExecutor extends Executor {
    private final ContentAPI contentApi;
    private final JobInfoCommand jobInfoCommand;

    public JobInfoExecutor(ContentAPI contentApi, JobInfoCommand jobInfoCommand) {
        this.contentApi = contentApi;
        this.jobInfoCommand = jobInfoCommand;
    }

    @Override
    public void execute() {
        try {
            Project project = contentApi.getProject(jobInfoCommand.getJobId());
            ProjectPresenter projectPresenter = new ProjectPresenter(project);

            out.println(getObjectMapper().writeValueAsString(projectPresenter));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
