package com.lionbridge.content.cli.job;

import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.TargetLanguage;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;
import static java.util.Collections.sort;
import static org.apache.commons.lang3.StringUtils.join;

public class ProjectPresenter {
    private final Project project;

    public ProjectPresenter(Project project) {
        this.project = project;
    }

    public String getStatus() {
        return project.getStatus();
    }

    public String getSourceFiles() {
        List<String> assetIds = new ArrayList<>();

        for (LBFile file : project.getFiles()) {
            assetIds.add(valueOf(file.getAssetId()));
        }

        sort(assetIds);

        return join(assetIds, ", ");
    }

    public String getTargetLanguages() {
        List<String> languageCodes = new ArrayList<>();

        List<TargetLanguage> targetLanguages = project.getTargetLanguages();

        for (TargetLanguage targetLanguage : targetLanguages) {
            languageCodes.add(targetLanguage.getLanguageCode());
        }

        sort(languageCodes);

        return join(languageCodes, ", ");
    }

    public String getCompletionDate() {
        return valueOf(project.getCompletionDate());
    }

    public String getDueDate() {
        return valueOf(project.getDueDate());
    }
}
