package com.lionbridge.content.cli.job;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.file.CliCommand;
import com.lionbridge.content.cli.validators.AssetIdListValidator;
import com.lionbridge.content.cli.validators.AssetIdValidator;

import java.util.ArrayList;
import java.util.List;

@CommandDefinition(name = "Add")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class JobAddCommand extends CliCommand {
    @Parameter(names = "--source-files", descriptionKey = "job.add.source-files", validateWith = AssetIdListValidator.class, required = true)
    private List<String> sourceFiles;

    @Parameter(names = "--source-locale", descriptionKey = "job.add.source-locale", required = true)
    private String sourceLocale;

    @Parameter(names = "--target-locales", descriptionKey = "job.add.target-locales", required = true)
    private List<String> targetLocales;

    @Parameter(names = "--reference-files", descriptionKey = "job.add.reference-files", validateWith = AssetIdListValidator.class)
    private List<String> referenceFiles;

    @Parameter(names = "--service", descriptionKey = "job.add.service", validateWith = AssetIdValidator.class, required = true)
    private Integer service;

    @Parameter(names = "--job-complete-endpoint", descriptionKey = "job.add.job-complete-endpoint")
    private List<String> jobCompleteEndPoints;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public ArrayList<String> getSourceFiles() {
        return (ArrayList<String>) sourceFiles;
    }

    public String getSourceLocale() {
        return sourceLocale;
    }

    public List<String> getTargetLocales() {
        return targetLocales;
    }

    public int getService() {
        return service;
    }

    public boolean isHelp() {
        return help;
    }

    public List<String> getReferenceFiles() {
        return referenceFiles;
    }

    public List<String> getJobCompleteEndPoints() {
        return jobCompleteEndPoints;
    }
}
