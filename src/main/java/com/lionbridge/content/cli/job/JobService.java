package com.lionbridge.content.cli.job;

import com.lionbridge.content.cli.BaseObjectService;
import com.lionbridge.content.cli.ObjectServiceDefinition;
import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;

import static java.lang.System.err;
import static java.lang.System.out;

@ObjectServiceDefinition(name = "Job")
public class JobService extends BaseObjectService {
    private TranslationOptionsService translationOptionsService = new TranslationOptionsService();

    @Override
    public void runCommand(String command, String[] commandParameters) {
        if ("add".equals(command)) {
            jobAdd(commandParameters);
        } else if ("info".equals(command)) {
            jobInfo(commandParameters);
        } else {
            err.println("Unsupported command.");
        }
    }
    
    @Override
    public void showHelp() {
        out.println("Valid commands are: add");
    }

    private void jobAdd(String[] commandParameters) {
        JobAddCommand jobAddCommand = new JobAddCommand();
        commander.addObject(jobAddCommand);
        commander.parse(commandParameters);

        if (jobAddCommand.isHelp()) {
            showObjectCommandHelp("job.add");
        } else {
            ContentAPI contentApi = getContentApi(jobAddCommand);

            JobAddExecutor jobAddExecutor = new JobAddExecutor(contentApi, jobAddCommand, translationOptionsService);
            jobAddExecutor.execute();
        }
    }

    private void jobInfo(String[] commandParameters) {
        JobInfoCommand jobInfoCommand = new JobInfoCommand();
        commander.addObject(jobInfoCommand);
        commander.parse(commandParameters);

        if (jobInfoCommand.isHelp()) {
            showObjectCommandHelp("job.info");
        } else {
            ContentAPI contentApi = getContentApi(jobInfoCommand);

            JobInfoExecutor jobInfoExecutor = new JobInfoExecutor(contentApi, jobInfoCommand);
            jobInfoExecutor.execute();
        }
    }
}
