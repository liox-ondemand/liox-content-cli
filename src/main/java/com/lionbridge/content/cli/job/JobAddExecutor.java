package com.lionbridge.content.cli.job;

import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Project;

import static java.lang.String.valueOf;
import static java.lang.System.out;

public class JobAddExecutor extends Executor {

    private JobAddCommand jobAddCommand;
    private ContentAPI contentApi;
    private TranslationOptionsService translationOptionsService;

    public JobAddExecutor(ContentAPI contentApi, JobAddCommand jobAddCommand, TranslationOptionsService translationOptionsService) {
        this.jobAddCommand = jobAddCommand;
        this.contentApi = contentApi;
        this.translationOptionsService = translationOptionsService;
    }

    public void execute() {
        try {
            Project project = contentApi.addProject(
                    "",
                    translationOptionsService.createJobAddTranslationOptions(jobAddCommand),
                    jobAddCommand.getSourceFiles(),
                    jobAddCommand.getReferenceFiles(),
                    jobAddCommand.getJobCompleteEndPoints()
            );

            out.println(valueOf(project.getProjectId()));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        }
    }
}
