package com.lionbridge.content.cli.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import static java.lang.Integer.parseInt;

public class AssetIdValidator implements IParameterValidator {
    @Override
    public void validate(String name, String assetId) throws ParameterException {
        try {
            parseInt(assetId);
        } catch(NumberFormatException numberFormatException) {
            throw new ParameterException("The ID provided for " + name + " is not a valid number.");
        }
    }
}
