package com.lionbridge.content.cli.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.util.Arrays;
import java.util.List;

import static java.lang.Integer.parseInt;

public class AssetIdListValidator implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        List<String> assetIds = Arrays.asList(value.split(","));

        for (String assetId: assetIds) {
            try {
                parseInt(assetId);
            } catch (NumberFormatException numberFormatException) {
                throw new ParameterException("The IDs provided for " + name + " contains an invalid number.");
            }
        }
    }
}
