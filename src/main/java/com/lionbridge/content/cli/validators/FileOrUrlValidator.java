package com.lionbridge.content.cli.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class FileOrUrlValidator implements IParameterValidator {
    @Override
    public void validate(String parameter, String path) throws ParameterException {
        if (isUrl(path)) {
            try {
                new URI(path);
            } catch (URISyntaxException e) {
                throw new ParameterException("URL provided for " + parameter + " is invalid.");
            }

        } else {
            File file = new File(path);

            if (!file.exists()) {
                throw new ParameterException("File provided for " + parameter + " does not exist.");
            }
        }
    }

    private boolean isUrl(String path) {
        if (path.startsWith("http://")) return true;
        if (path.startsWith("https://")) return true;
        if (path.startsWith("ftp://")) return true;
        if (path.startsWith("sftp://")) return true;

        return false;
    }

}
