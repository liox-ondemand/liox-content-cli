package com.lionbridge.content.cli.validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

import java.io.File;

public class DirectoryValidator implements IParameterValidator {
    @Override
    public void validate(String name, String path) throws ParameterException {
        File targetDirectory = new File(path);

        if (!targetDirectory.isDirectory()) {
            throw new ParameterException("Output directory '" + path + "' does not exist.");
        }
    }
}
