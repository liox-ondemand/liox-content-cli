package com.lionbridge.content.cli;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.LBError;

import static java.lang.System.err;
import static java.lang.System.exit;

public abstract class Executor {
    public abstract void execute();

    protected void handleContentApiException(ContentAPIException contentApiException) {
        if (contentApiException.getErrors().size() > 0) {
            printExceptionErrorMessages(contentApiException);
        } else {
            err.println(contentApiException.getMessage());
        }

        exit(1);
    }

    private void printExceptionErrorMessages(ContentAPIException contentApiException) {
        for (LBError error : contentApiException.getErrors()) {
            err.println(error.getSimpleMessage());
        }
    }

    protected ObjectMapper getObjectMapper() {
        YAMLFactory yamlFactory = new YAMLFactory();
        yamlFactory.disable(YAMLGenerator.Feature.SPLIT_LINES);
        yamlFactory.disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);
        yamlFactory.enable(YAMLGenerator.Feature.MINIMIZE_QUOTES);

        return new ObjectMapper(yamlFactory);
    }
}
