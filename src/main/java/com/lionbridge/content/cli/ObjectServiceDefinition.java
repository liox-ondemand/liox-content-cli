package com.lionbridge.content.cli;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This annotation is used:
 * - by the documentation script to supply a list of supported Objects
 * - by the ObjectRegistry to automatically register ObjectServices
 */
@Retention(RUNTIME)
@Target(value = TYPE)
public @interface ObjectServiceDefinition {
    String name();
}