package com.lionbridge.content.cli.file;

import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.out;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileDownloadTargetsExecutor extends Executor {
    private final ContentAPI contentApi;
    private final FileDownloadTargetsCommand fileDownloadTargetsCommand;

    public FileDownloadTargetsExecutor(ContentAPI contentApi, FileDownloadTargetsCommand fileDownloadTargetsCommand) {
        this.contentApi = contentApi;
        this.fileDownloadTargetsCommand = fileDownloadTargetsCommand;
    }

    @Override
    public void execute() {
        try {
            String targetDirectory = fileDownloadTargetsCommand.getOutputDirectory();

            File translatedFile = contentApi.getTranslatedFile(
                    fileDownloadTargetsCommand.getAssetId(),
                    fileDownloadTargetsCommand.getTargetLocale()
            );

            Files.move(
                    translatedFile.toPath(),
                    Paths.get(targetDirectory + File.separator + translatedFile.getName()),
                    REPLACE_EXISTING
            );

            out.println("Success!");
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
