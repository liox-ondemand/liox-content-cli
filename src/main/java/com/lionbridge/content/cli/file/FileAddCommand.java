package com.lionbridge.content.cli.file;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.validators.FileOrUrlValidator;

@CommandDefinition(name = "Add")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class FileAddCommand extends CliCommand {
    @Parameter(names = {"--file", "--local-file"}, descriptionKey = "file.add.file", validateWith = FileOrUrlValidator.class, required = true)
    private String file;

    @Parameter(names = "--source-locale", descriptionKey = "file.add.source-locale", required = true)
    private String sourceLocale;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getFile() {
        return file;
    }

    public String getSourceLocale() {
        return sourceLocale;
    }

    public boolean isHelp() {
        return help;
    }

}
