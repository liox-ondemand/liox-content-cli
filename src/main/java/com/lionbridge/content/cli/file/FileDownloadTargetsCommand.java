package com.lionbridge.content.cli.file;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.validators.AssetIdValidator;
import com.lionbridge.content.cli.validators.DirectoryValidator;

@CommandDefinition(name = "Download-Targets")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class FileDownloadTargetsCommand extends CliCommand {
    @Parameter(names = "--file", descriptionKey = "file.download-targets.file", validateWith = AssetIdValidator.class, required = true)
    private String assetId;

    @Parameter(names = "--target-locale", descriptionKey = "file.download-targets.target-locale", required = true)
    private String targetLocale;

    @Parameter(names = "--output-dir", descriptionKey = "file.download-targets.output-dir", validateWith = DirectoryValidator.class, required = true)
    private String outputDirectory;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getAssetId() {
        return assetId;
    }

    public String getTargetLocale() {
        return targetLocale;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public boolean isHelp() {
        return help;
    }
}
