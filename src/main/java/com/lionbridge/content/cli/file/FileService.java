package com.lionbridge.content.cli.file;

import com.lionbridge.content.cli.BaseObjectService;
import com.lionbridge.content.cli.ObjectServiceDefinition;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.utilities.MimeTypeMapper;

import static java.lang.System.err;
import static java.lang.System.out;

@ObjectServiceDefinition(name = "File")
public class FileService extends BaseObjectService {
    private MimeTypeMapper mimeTypeMapper = new MimeTypeMapper();

    @Override
    public void runCommand(String command, String[] commandParameters) {
        if ("add".equals(command)) {
            fileAdd(commandParameters);
        } else if ("info".equals(command)) {
            fileInfo(commandParameters);
        } else if ("download-targets".equals(command)) {
            fileDownloadTargets(commandParameters);
        } else {
            err.println("Unsupported command.");
        }
    }

    @Override
    public void showHelp() {
        out.println("Valid commands are: add, info, download-targets");
    }

    private void fileAdd(String[] commandParameters) {
        FileAddCommand fileAddCommand = new FileAddCommand();
        commander.addObject(fileAddCommand);
        commander.parse(commandParameters);

        if (fileAddCommand.isHelp()) {
            showObjectCommandHelp("file.add");
        } else {
            ContentAPI contentApi = getContentApi(fileAddCommand);

            FileAddExecutor fileAddExecutor = new FileAddExecutor(mimeTypeMapper, contentApi, fileAddCommand);
            fileAddExecutor.execute();
        }
    }

    private void fileInfo(String[] commandParameters) {
        FileInfoCommand fileInfoCommand = new FileInfoCommand();
        commander.addObject(fileInfoCommand);
        commander.parse(commandParameters);

        if (fileInfoCommand.isHelp()) {
            showObjectCommandHelp("file.info");
        } else {
            ContentAPI  contentApi = getContentApi(fileInfoCommand);

            FileInfoExecutor fileInfoExecutor = new FileInfoExecutor(contentApi, fileInfoCommand);
            fileInfoExecutor.execute();
        }
    }

    private void fileDownloadTargets(String[] commandParameters) {
        FileDownloadTargetsCommand fileDownloadTargetsCommand = new FileDownloadTargetsCommand();
        commander.addObject(fileDownloadTargetsCommand);
        commander.parse(commandParameters);

        if (fileDownloadTargetsCommand.isHelp()) {
            showObjectCommandHelp("file.download-targets");
        } else {
            ContentAPI  contentApi = getContentApi(fileDownloadTargetsCommand);

            FileDownloadTargetsExecutor fileDownloadTargetsExecutor = new FileDownloadTargetsExecutor(contentApi, fileDownloadTargetsCommand);
            fileDownloadTargetsExecutor.execute();
        }
    }

}
