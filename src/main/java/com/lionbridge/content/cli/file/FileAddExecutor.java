package com.lionbridge.content.cli.file;

import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.utilities.MimeTypeMapper;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

import static java.lang.String.valueOf;
import static java.lang.System.out;

public class FileAddExecutor extends Executor {
    private FileAddCommand fileAddCommand;
    private ContentAPI contentApi;
    private MimeTypeMapper mimeTypeMapper;

    public FileAddExecutor(MimeTypeMapper mimeTypeMapper, ContentAPI contentApi, FileAddCommand fileAddCommand) {
        this.mimeTypeMapper = mimeTypeMapper;
        this.fileAddCommand = fileAddCommand;
        this.contentApi = contentApi;
    }

    public void execute() {
        try {
            String file = fileAddCommand.getFile();
            String mimeType = mimeTypeMapper.getMimeType(file);
            String languageCode = fileAddCommand.getSourceLocale();

            LBFile lbFile = null;

            if (isUrl(file)) {
                URI uri = null;

                try {
                    uri = new URI(file);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                lbFile = contentApi.addFileByReference(uri, languageCode);

            } else {
                File inputFile = new File(file);

                lbFile = contentApi.addFile(mimeType, inputFile, languageCode);

            }

            out.println(valueOf(lbFile.getAssetId()));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        }
    }

    private boolean isUrl(String file) {
        if (file.startsWith("http://")) return true;
        if (file.startsWith("https://")) return true;
        if (file.startsWith("ftp://")) return true;
        if (file.startsWith("sftp://")) return true;

        return false;
    }
}
