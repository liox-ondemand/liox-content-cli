package com.lionbridge.content.cli.file;

import com.lionbridge.content.sdk.models.LBFile;

import static java.lang.String.valueOf;

public class FilePresenter {
    private LBFile file;

    public FilePresenter(LBFile file) {
        this.file = file;
    }

    public String getStatus() {
        return file.getFileStatusTemp();
    }

    public String getAssetId() {
        return valueOf(file.getAssetId());
    }

    public String getName() {
        return file.getName();
    }

    public String getProjectId() {
        return valueOf(file.getProjectId());
    }

    public String getSourceLanguage() {
        return file.getSourceLanguage().getLanguageCode();
    }
}
