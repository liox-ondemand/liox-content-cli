package com.lionbridge.content.cli.file;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.lionbridge.content.cli.Executor;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.LBFile;

import static java.lang.System.out;

public class FileInfoExecutor extends Executor {
    private final ContentAPI contentApi;
    private final FileInfoCommand fileInfoCommand;

    public FileInfoExecutor(ContentAPI contentApi, FileInfoCommand fileInfoCommand) {
        this.contentApi  = contentApi;
        this.fileInfoCommand = fileInfoCommand;
    }

    @Override
    public void execute() {
        try {
            LBFile file = contentApi.getFileDetails(fileInfoCommand.getFileId());
            FilePresenter filePresenter = new FilePresenter(file);

            out.println(getObjectMapper().writeValueAsString(filePresenter));
        } catch (ContentAPIException contentApiException) {
            handleContentApiException(contentApiException);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
