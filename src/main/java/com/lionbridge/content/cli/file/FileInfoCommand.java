package com.lionbridge.content.cli.file;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.lionbridge.content.cli.CommandDefinition;
import com.lionbridge.content.cli.validators.AssetIdValidator;

@CommandDefinition(name = "Info")
@Parameters(separators = "=", resourceBundle = "MessagesBundle")
public class FileInfoCommand extends CliCommand {
    @Parameter(names = "--file", descriptionKey = "file.info.file", validateWith = AssetIdValidator.class, required = true)
    private String fileId;

    @Parameter(names = "--help", descriptionKey = "help", help = true)
    private boolean help;

    public String getFileId() {
        return fileId;
    }

    public boolean isHelp() {
        return help;
    }
}
