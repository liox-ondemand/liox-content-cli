package com.lionbridge.content.cli.file;

import com.beust.jcommander.Parameter;

public abstract class CliCommand {
    @Parameter(names = "--access-key-id", descriptionKey = "global.access-key-id")
    private String apiAccessKeyId;

    @Parameter(names = "--secret-key", descriptionKey = "global.secret-key")
    private String apiSecretkey;

    @Parameter(names = "--endpoint", descriptionKey = "global.endpoint")
    private String apiEndPoint;

    @Parameter(names = "--default-currency", descriptionKey = "global.default-currency")
    private String apiDefaultCurrency;

    public String getApiAccessKeyId() {
        return apiAccessKeyId;
    }

    public String getApiSecretkey() {
        return apiSecretkey;
    }

    public String getApiEndPoint() {
        return apiEndPoint;
    }

    public String getApiDefaultCurrency() {
        return apiDefaultCurrency;
    }
}
