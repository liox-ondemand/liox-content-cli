package com.lionbridge.content.cli;

import static java.lang.System.exit;
import static java.lang.System.out;

/**
 * This is the main class for the Lionbridge Content CLI
 */
public class CommandLineInterface {
    private CommandLineInterface() { }

    /**
     * The main entry point
     *
     * @param arguments Command-line arguments for the content CLI
     */
    public static void main(String[] arguments) {
        Runner runner = new Runner();

        try {
            runner.run(arguments);
        } catch (RuntimeException e) {
            out.println(e.getMessage());
            exit(1);
        }
    }
}
