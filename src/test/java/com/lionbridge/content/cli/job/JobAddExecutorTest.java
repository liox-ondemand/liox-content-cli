package com.lionbridge.content.cli.job;

import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.TranslationOptions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.when;

public class JobAddExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private JobAddCommand jobAddCommand;

    @Mock
    private List<String> endpoints;

    @Mock
    private Project project;

    @Mock
    private TranslationOptionsService translationOptionsService;

    @Mock
    private TranslationOptions translationOptions;

    @Mock
    private ArrayList<String> fileAssetIds;

    @Mock
    private ArrayList<String> referenceFileAssetIds;

    @Before
    public void before() throws Exception {
        initMocks(this);

        when(translationOptionsService.createJobAddTranslationOptions(jobAddCommand)).thenReturn(translationOptions);
        when(jobAddCommand.getSourceFiles()).thenReturn(fileAssetIds);
        when(jobAddCommand.getReferenceFiles()).thenReturn(referenceFileAssetIds);
        when(jobAddCommand.getJobCompleteEndPoints()).thenReturn(endpoints);
        when(contentApi.addProject("", translationOptions, fileAssetIds, referenceFileAssetIds, endpoints)).thenReturn(project);
        when(project.getProjectId()).thenReturn(42);
    }

    @Test
    public void execute_withValidParameters_returnsJobId() throws ContentAPIException {
        JobAddExecutor jobAddExecutor = new JobAddExecutor(contentApi, jobAddCommand, translationOptionsService);

        jobAddExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("42\n"));
    }
}