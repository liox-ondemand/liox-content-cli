package com.lionbridge.content.cli.job;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class JobAddCommandTest {
    private JobAddCommand jobAddCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        jobAddCommand = new JobAddCommand();
        commander = new JCommander(jobAddCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse(
                "--source-files=999,1000",
                "--source-locale=en-us",
                "--target-locales=fr-fr,it-it,es-es",
                "--reference-files=321,654",
                "--service=10",
                "--job-complete-endpoint=foo@bar.com,http://foo.com/bar"
        );

        assertThat(jobAddCommand.getTargetLocales(), hasItems("fr-fr", "it-it" ,"es-es"));
        assertThat(jobAddCommand.getSourceFiles(), hasItems("999", "1000"));
        assertThat(jobAddCommand.getSourceLocale(), equalTo("en-us"));
        assertThat(jobAddCommand.getReferenceFiles(), hasItems("321", "654"));
        assertThat(jobAddCommand.getService(), equalTo(10));
        assertThat(jobAddCommand.getJobCompleteEndPoints().size(), equalTo(2));

    }

    @Test
    public void requiresSourceFilesParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --source-files"));

        commander.parse(
                "--source-locale=en-us",
                "--target-locales=fr-fr,it-it,es-es",
                "--service=10"
        );
    }

    @Test
    public void requiresSourceLocaleParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --source-locale"));

        commander.parse(
                "--source-files=999,1000",
                "--target-locales=fr-fr,it-it,es-es",
                "--service=10"
        );
    }

    @Test
    public void requiresTargetLocalesParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --target-locales"));

        commander.parse(
                "--source-files=999,1000",
                "--source-locale=en-us",
                "--service=10"
        );
    }

    @Test
    public void requiresServiceParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --service"));

        commander.parse(
                "--source-files=999,1000",
                "--source-locale=en-us",
                "--target-locales=fr-fr,it-it,es-es"
        );
    }
}