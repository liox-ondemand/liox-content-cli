package com.lionbridge.content.cli.job;

import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.TargetLanguage;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ProjectPresenterTest {
    private ProjectPresenter projectPresenter;

    private Project project = new Project();

    @Before
    public void before() {
        Date completionDate = new Date();
        Date dueDate = new Date();

        LBFile lbFile1 = new LBFile();
        lbFile1.setAssetId(42);
        LBFile lbFile2 = new LBFile();
        lbFile2.setAssetId(43);

        List<LBFile> files = new ArrayList<>();
        files.add(lbFile1);
        files.add(lbFile2);

        List<TargetLanguage> targetLanguages = new ArrayList<>();
        targetLanguages.add(new TargetLanguage("en-us"));
        targetLanguages.add(new TargetLanguage("de-de"));

        project.setStatus("New");
        project.setCompletionDate(completionDate);
        project.setDueDate(dueDate);
        project.setFiles(files);

        project.setTargetLanguages(targetLanguages);
        projectPresenter = new ProjectPresenter(project);
    }

    @Test
    public void getStatus_returnsProjectStatus() {
        assertThat(projectPresenter.getStatus(), equalTo("New"));
    }

    @Test
    public void getSourceFiles_returnsCommaSeparatedListOfSortedAssetIds() {
        assertThat(projectPresenter.getSourceFiles(), equalTo("42, 43"));
    }

    @Test
    public void getTargetLanguages_returnsCommaSeparatedListOfSortedTargetLanguageCodes() {
        assertThat(projectPresenter.getTargetLanguages(), equalTo("de-de, en-us"));
    }

}