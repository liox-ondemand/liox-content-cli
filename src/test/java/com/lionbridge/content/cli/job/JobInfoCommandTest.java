package com.lionbridge.content.cli.job;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

public class JobInfoCommandTest {
    private JobInfoCommand jobInfoCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        jobInfoCommand = new JobInfoCommand();
        commander = new JCommander(jobInfoCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse("--job", "42");

        assertEquals(jobInfoCommand.getJobId(), "42");
    }

    @Test
    public void requiresJobParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --job"));

        commander.parse();
    }
}