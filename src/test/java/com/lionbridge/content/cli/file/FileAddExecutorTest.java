package com.lionbridge.content.cli.file;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.utilities.MimeTypeMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.net.URI;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { FileAddExecutor.class, File.class } )
public class FileAddExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Mock
    private MimeTypeMapper mimeTypeMapper;

    @Mock
    private ContentAPI contentApi;

    @Mock
    private FileAddCommand fileAddCommand;

    @Mock
    private LBFile lbFile;

    @Mock
    private File inputFile;

    @Before
    public void before() throws Exception {
        when(fileAddCommand.getSourceLocale()).thenReturn("en-us");
        when(lbFile.getAssetId()).thenReturn(42);
    }

    @Test
    public void execute_withValidFileArguments_returnsAssetId() throws Exception {
        whenNew(File.class).withArguments("xmlFile").thenReturn(inputFile);
        when(fileAddCommand.getFile()).thenReturn("xmlFile");
        when(mimeTypeMapper.getMimeType("xmlFile")).thenReturn("text/xml");
        when(contentApi.addFile("text/xml", inputFile, "en-us")).thenReturn(lbFile);

        FileAddExecutor fileAddExecutor = new FileAddExecutor(
                mimeTypeMapper,
                contentApi,
                fileAddCommand
        );

        fileAddExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("42\n"));
    }

    @Test
    public void execute_withValidUrlArguments_returnAssetId() throws Exception {
        String url = "https://bitbucket.org/liox-ondemand/liox-content-sdk-java/raw/3d1ea2817e1db258006f7039c0242bcc1f2aa4a4/LICENSE.txt";
        when(fileAddCommand.getFile()).thenReturn(url);
        URI uri = new URI(url);
        when(contentApi.addFileByReference(any(URI.class), any(String.class))).thenReturn(lbFile);

        FileAddExecutor fileAddExecutor = new FileAddExecutor(
                mimeTypeMapper,
                contentApi,
                fileAddCommand
        );

        fileAddExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("42\n"));
    }

    @Test
    public void fileAdd_withUnsupportedContentType_returnsNonZeroExitCode() throws Exception {
        whenNew(File.class).withArguments("markdownFile").thenReturn(inputFile);
        when(fileAddCommand.getFile()).thenReturn("markdownFile");
        when(mimeTypeMapper.getMimeType("markdownFile")).thenReturn(null);
        when(contentApi.addFile(null, inputFile, "en-us")).thenThrow(new ContentAPIException());

        final FileAddExecutor fileAddExecutor = new FileAddExecutor(
                mimeTypeMapper,
                contentApi,
                fileAddCommand
        );

        exit.expectSystemExitWithStatus(1);
        fileAddExecutor.execute();
    }
}