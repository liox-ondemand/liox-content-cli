package com.lionbridge.content.cli.file;

import com.lionbridge.content.sdk.models.LBFile;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class FilePresenterTest {
    @Mock
    LBFile lbFile;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        when(lbFile.getAssetId()).thenReturn(42);
    }

    @Test
    public void getAssetId_returnsAssetIdAsString() {
        FilePresenter filePresenter = new FilePresenter(lbFile);

        assertThat(filePresenter.getAssetId(), equalTo("42"));
    }

}