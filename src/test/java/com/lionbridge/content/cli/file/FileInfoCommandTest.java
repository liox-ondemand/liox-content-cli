package com.lionbridge.content.cli.file;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

public class FileInfoCommandTest {
    private FileInfoCommand fileInfoCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        fileInfoCommand = new FileInfoCommand();
        commander = new JCommander(fileInfoCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse("--file", "42");

        assertEquals(fileInfoCommand.getFileId(), "42");
    }

    @Test
    public void requiresFileParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --file"));

        commander.parse();
    }
}