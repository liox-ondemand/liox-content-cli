package com.lionbridge.content.cli.file;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class FileDownloadTargetsCommandTest {
    private FileDownloadTargetsCommand fileDownloadTargetsCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        fileDownloadTargetsCommand = new FileDownloadTargetsCommand();
        commander = new JCommander(fileDownloadTargetsCommand);
    }

    @Test
    public void parsesParamatersAndAssignsValues() {
        commander.parse(
                "--file=42",
                "--output-dir=./target",
                "--target-locale=fr-fr"
        );

        assertThat(fileDownloadTargetsCommand.getAssetId(), equalTo("42"));
        assertThat(fileDownloadTargetsCommand.getOutputDirectory(), equalTo("./target"));
        assertThat(fileDownloadTargetsCommand.getTargetLocale(), equalTo("fr-fr"));
    }

    @Test
    public void requiresFileParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --file"));

        commander.parse(
                "--output-dir=./target",
                "--target-locale=fr-fr"
        );
    }

    @Test
    public void requiresOutputDirParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --output-dir"));

        commander.parse(
                "--file=42",
                "--target-locale=fr-fr"
        );
    }

    @Test
    public void requiresTargetLocaleParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --target-locale"));

        commander.parse(
                "--file=42",
                "--output-dir=./target"
        );
    }
}