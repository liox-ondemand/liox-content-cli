package com.lionbridge.content.cli.file;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

public class FileAddCommandTest {
    private FileAddCommand fileAddCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        fileAddCommand = new FileAddCommand();
        commander = new JCommander(fileAddCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse("--file", "./pom.xml", "--source-locale", "en-us");

        assertEquals(fileAddCommand.getFile(), "./pom.xml");
        assertEquals(fileAddCommand.getSourceLocale(), "en-us");
    }

    @Test
    public void requiresFileParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --file"));

        commander.parse("--source-locale", "en-us");
    }

    @Test
    public void requiresSourceLocaleParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --source-locale"));

        commander.parse("--file", "./pom.xml");
    }
}