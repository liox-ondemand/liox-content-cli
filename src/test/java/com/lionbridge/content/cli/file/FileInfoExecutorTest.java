package com.lionbridge.content.cli.file;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.models.LBFile;
import com.lionbridge.content.sdk.models.SourceLanguage;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest( { FileInfoExecutor.class, File.class } )
public class FileInfoExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private FileInfoCommand fileInfoCommand;

    @Mock
    private LBFile lbFile;

    @Before
    public void before() throws Exception {
        when(fileInfoCommand.getFileId()).thenReturn("42");

        when(lbFile.getFileStatusTemp()).thenReturn("Status Temp");
        when(lbFile.getSourceLanguage()).thenReturn(new SourceLanguage("fr-fr"));

        when(contentApi.getFileDetails("42")).thenReturn(lbFile);
    }

    @Test
    public void execute_withValidArguments_returnsAssetId() {
        FileInfoExecutor fileAddExecutor = new FileInfoExecutor(contentApi, fileInfoCommand);

        fileAddExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("Status Temp\n"));
        assertThat(systemOutRule.getLog(), containsString("fr-fr\n"));
    }
}