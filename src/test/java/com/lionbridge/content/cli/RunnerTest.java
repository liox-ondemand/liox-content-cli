package com.lionbridge.content.cli;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.Assertion;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.contrib.java.lang.system.SystemErrRule;
import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.File;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class RunnerTest {
    private Runner runner;

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Rule
    public final SystemErrRule systemErrRule = new SystemErrRule().enableLog().muteForSuccessfulTests();

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void before() {
        runner = new Runner();
    }

    @Test
    public void journeyTest() {
        runner.run("file", "add", "--file", "./src/test/resources/example.xml", "--source-locale", "en-us");

        String fileId = getSystemOutLogWithoutNewlines();

        systemOutRule.clearLog();
        runner.run("file", "add", "--file", "https://bitbucket.org/liox-ondemand/liox-content-sdk-java/raw/3d1ea2817e1db258006f7039c0242bcc1f2aa4a4/LICENSE.txt", "--source-locale", "en-us");

        String file2Id = getSystemOutLogWithoutNewlines();

        systemOutRule.clearLog();
        runner.run("file", "add", "--local-file", "./src/test/resources/example.txt", "--source-locale", "en-us");

        String referenceFileId = getSystemOutLogWithoutNewlines();

        systemOutRule.clearLog();
        runner.run("file", "info", "--file=" + fileId);

        String fileInfo = getSystemOutLogWithoutNewlines();

        assertThat(fileInfo, containsString("status: Analyzed"));

        systemOutRule.clearLog();
        runner.run("job", "add",
                "--source-files=" + fileId + "," + file2Id,
                "--source-locale=en-us",
                "--target-locales=fr-fr,it-it,es-es",
                "--reference-files=" + referenceFileId,
                "--service=146",
                "--job-complete-endpoint=foo@bar.com,http://foo.com/bar"
        );

        String jobId = getSystemOutLogWithoutNewlines();

        systemOutRule.clearLog();
        runner.run("job", "info", "--job=" + jobId);

        String jobInfo = getSystemOutLogWithoutNewlines();
        assertThat(jobInfo, containsString("status: New"));

        systemOutRule.clearLog();
        runner.run("quote", "add", "--jobs=" + jobId, "--currency=USD --quote-ready-endpoint=foo@bar.com,http://foo.com/bar");

        String quoteId = getSystemOutLogWithoutNewlines();

        systemOutRule.clearLog();
        runner.run("quote", "info", "--quote=" + quoteId);

        String quoteInfo = getSystemOutLogWithoutNewlines();
        assertThat(quoteInfo, containsString("status: Pending"));

        systemOutRule.clearLog();
        runner.run("quote", "authorize",
                "--quote=" + quoteId,
                "--po-number=123456",
                "--quote-paid-endpoint=foo@bar.com,http://foo.com/bar"
        );

        String status = getSystemOutLogWithoutNewlines();

        assertThat(status, equalTo("Success!"));
    }

    @Test
    public void run_withEnvironmentVariablesOnCommandLine() {
        exit.expectSystemExitWithStatus(1);
        exit.checkAssertionAfterwards(new Assertion() {
            public void checkAssertion() {
                assertThat(systemOutRule.getLog(), containsString("Unable to connect to API. Please check the credentials and endpoint used."));
            }
        });

        runner.run(
            "file", "add",
            "--file", "./src/test/resources/example.xml",
            "--source-locale", "en-us",
            "--access-key-id", "foo",
            "--secret-key", "bar",
            "--endpoint", "https://developer-sandbox.liondemand.com/api",
            "--default-currency", "USD"
        );
    }

    @Test
    @Ignore("This needs to be made more robust.")
    public void run_downloadTargets() {
        runner.run("file", "download-targets", "--file=286687", "--target-locale=fr-fr", "--output-dir=./target");

        String status = getSystemOutLogWithoutNewlines();
        assertThat(status, equalTo("Success!"));

        File frenchFile = new File("target/example.xml");
        assertThat(frenchFile.exists(), equalTo(Boolean.TRUE));
    }

    @Test
    public void fileAdd_withInvalidLocale_showsPrettyError() {
        exit.expectSystemExitWithStatus(1);
        exit.checkAssertionAfterwards(new Assertion() {
            public void checkAssertion() {
                assertThat(systemErrRule.getLog(), not(containsString("ContentAPIException")));
                assertThat(systemErrRule.getLog(), containsString("Locale matching query does not exist. foo-bar"));
            }
        });

        runner.run("file", "add", "--local-file", "./src/test/resources/example.xml", "--source-locale", "foo-bar");
    }

    @Test
    public void jobAdd_withInvalidFile_showsPrettyError() {
        exit.expectSystemExitWithStatus(1);
        exit.checkAssertionAfterwards(new Assertion() {
            public void checkAssertion() {
                assertThat(systemErrRule.getLog(), not(containsString("ContentAPIException")));
                assertThat(systemErrRule.getLog(), containsString("No File found with asset id = 0"));
            }
        });

        runner.run("job", "add",
                "--source-files=0",
                "--source-locale=en-us",
                "--target-locales=fr-fr,it-it,es-es",
                "--service=146"
        );
    }

    @Test
    public void run_withoutArguments_displaysGeneralHelp() {
        runner.run();

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans <object> <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid objects are: file, job, locale, quote, service"));
    }

    @Test
    public void run_withHelpArgument_displaysGeneralHelp() {
        runner.run("--help");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans <object> <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid objects are: file, job, locale, quote, service"));
    }

    @Test
    public void run_withVersionArgument_displaysGeneralHelp() {
        runner.run("--version");

        assertThat(systemOutRule.getLog(), containsString("Lionbridge Content CLI version"));
    }

    @Test
    public void run_withFileObject_displaysFileHelp() {
        runner.run("file");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans file <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: add"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans file <command> --help"));
    }

    @Test
    public void run_withFileObjectAndHelpArgument_displaysFileHelp() {
        runner.run("file", "--help");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans file <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: add"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans file <command> --help"));
    }

    @Test
    public void run_withJobObject_displaysJobHelp() {
        runner.run("job");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans job <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: add"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans job <command> --help"));
    }

    @Test
    public void run_withQuoteObject_displaysQuoteHelp() {
        runner.run("quote");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans quote <command>"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: add, authorize"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans quote <command> --help"));
    }

    @Test
    public void run_withLocaleObject_displaysLocaleHelp() {
        runner.run("locale");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans locale <command>\n"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: list"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans locale <command> --help"));
    }

    @Test
    public void run_withServiceObject_displaysServiceHelp() {
        runner.run("service");

        assertThat(systemOutRule.getLog(), containsString("usage: ltrans service <command>\n"));
        assertThat(systemOutRule.getLog(), containsString("Valid commands are: list"));
        assertThat(systemOutRule.getLog(), containsString("To display help for a specific command use: ltrans service <command> --help"));
    }

    @Test
    public void run_withUnknownObject_displaysQuoteHelp() {
        runner.run("pizza");

        assertThat(systemErrRule.getLog(), containsString("Invalid object type."));
        assertThat(systemErrRule.getLog(), containsString("Valid objects are: file, job, locale, quote, service"));
    }

    private String getSystemOutLogWithoutNewlines() {
        return systemOutRule.getLog().replaceAll("\\r|\\n", "");
    }
}
