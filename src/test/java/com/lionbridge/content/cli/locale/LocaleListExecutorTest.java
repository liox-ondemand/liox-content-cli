package com.lionbridge.content.cli.locale;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Locale;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class LocaleListExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private Locale locale;

    @Before
    public void before() throws ContentAPIException {
        initMocks(this);

        when(locale.getCode()).thenReturn("en-us");
        when(locale.getName()).thenReturn("English (United States)");

        List<Locale> locales = new ArrayList<>(asList(locale));

        when(contentApi.getLocales()).thenReturn(locales);
    }

    @Test
    public void execute_returnsListOfLocales() {
        LocaleListExecutor localeListExecutor = new LocaleListExecutor(contentApi);

        localeListExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("locales:"));
        assertThat(systemOutRule.getLog(), containsString(" code: en-us"));
        assertThat(systemOutRule.getLog(), containsString(" name: English (United States)\n"));
    }
}