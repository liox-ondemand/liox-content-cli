package com.lionbridge.content.cli.service;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

public class ServiceInfoCommandTest {
    private ServiceInfoCommand serviceInfoCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        serviceInfoCommand = new ServiceInfoCommand();
        commander = new JCommander(serviceInfoCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse("--service", "42");

        assertEquals(serviceInfoCommand.getServiceId(), "42");
    }

    @Test
    public void requiresFileParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --service"));

        commander.parse();
    }
}