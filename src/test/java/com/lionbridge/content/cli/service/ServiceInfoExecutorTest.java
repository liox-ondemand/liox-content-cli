package com.lionbridge.content.cli.service;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Service;
import com.lionbridge.content.sdk.models.SourceLanguage;
import com.lionbridge.content.sdk.models.TargetLanguage;
import com.lionbridge.content.sdk.models.ValidInputs;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ServiceInfoExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private ServiceInfoCommand serviceInfoCommand;

    @Before
    public void before() throws ContentAPIException {
        initMocks(this);

        when(serviceInfoCommand.getServiceId()).thenReturn("42");

        List<SourceLanguage> sourceLanguages = new ArrayList<>();
        sourceLanguages.add(new SourceLanguage("ar-ma"));
        sourceLanguages.add(new SourceLanguage("ar-ae"));

        List<TargetLanguage> targetLanguages = new ArrayList<>();
        targetLanguages.add(new TargetLanguage("zh-cn"));
        targetLanguages.add(new TargetLanguage("bg-bg"));

        ValidInputs validInputs = new ValidInputs();
        validInputs.setFiles(Arrays.asList("pptx", "csv", "docx"));

        Service service = new Service();
        service.setName("Pseudo Translation");
        service.setServiceId(42);
        service.setSourceLanguages(sourceLanguages);
        service.setTargetLanguages(targetLanguages);
        service.setValidInputs(validInputs);

        when(contentApi.getService(42)).thenReturn(service);
    }

    @Test
    public void execute_returnsServiceInfo() {
        ServiceInfoExecutor serviceInfoExecutor = new ServiceInfoExecutor(contentApi, serviceInfoCommand);

        serviceInfoExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("name: Pseudo Translation"));
        assertThat(systemOutRule.getLog(), containsString("id: 42"));
        assertThat(systemOutRule.getLog(), containsString("validTypes: csv, docx, pptx"));
        assertThat(systemOutRule.getLog(), containsString("sourceLanguages: ar-ae, ar-ma"));
        assertThat(systemOutRule.getLog(), containsString("targetLanguages: bg-bg, zh-cn"));
        assertThat(systemOutRule.getLog(), not(containsString("priceDescription:")));
    }
}