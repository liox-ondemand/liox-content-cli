package com.lionbridge.content.cli.service;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.ContentAPIException;
import com.lionbridge.content.sdk.models.Service;
import com.lionbridge.content.sdk.models.ValidInputs;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ServiceListExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Before
    public void before() throws ContentAPIException {
        initMocks(this);

        Service service = new Service();
        service.setName("Pseudo Translation");
        service.setServiceId(42);
        ValidInputs validInputs = new ValidInputs();
        validInputs.setFiles(asList("pptx", "csv", "docx"));
        service.setValidInputs(validInputs);

        List<Service> services = new ArrayList<>(asList(service));

        when(contentApi.getServices()).thenReturn(services);
    }

    @Test
    public void execute_returnsListOfServices() {
        ServiceListExecutor serviceListExecutor = new ServiceListExecutor(contentApi);

        serviceListExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("services:"));
        assertThat(systemOutRule.getLog(), containsString(" name: Pseudo Translation"));
        assertThat(systemOutRule.getLog(), containsString(" id: 42"));
        assertThat(systemOutRule.getLog(), containsString(" validTypes: csv, docx, pptx"));
        assertThat(systemOutRule.getLog(), not(containsString(" priceDescription:")));
    }

}