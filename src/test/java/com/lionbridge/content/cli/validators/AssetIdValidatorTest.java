package com.lionbridge.content.cli.validators;

import com.beust.jcommander.ParameterException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;

public class AssetIdValidatorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private AssetIdValidator assetIdValidator = new AssetIdValidator();

    @Test
    public void validate_withValidNumber_doesNotThrowException() {
        assetIdValidator.validate("name", "123");
    }

    @Test
    public void validate_withInvalidNumber_throwsException() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The ID provided for --name is not a valid number."));

        assetIdValidator.validate("--name", "123foo");
    }
}