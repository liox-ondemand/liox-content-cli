package com.lionbridge.content.cli.validators;

import com.beust.jcommander.ParameterException;
import com.lionbridge.content.cli.validators.AssetIdListValidator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;

public class AssetIdListValidatorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private AssetIdListValidator assetIdListValidator = new AssetIdListValidator();

    @Test
    public void validate_withValidNumbers_doesNotThrowException() {
        assetIdListValidator.validate("--names", "123,456");
    }

    @Test
    public void validate_withInvalidNumber_throwsException() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The IDs provided for --names contains an invalid number."));

        assetIdListValidator.validate("--names", "123,456foo");
    }
}