package com.lionbridge.content.cli.validators;

import com.beust.jcommander.ParameterException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;

public class FileOrUrlValidatorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private FileOrUrlValidator fileOrUrlValidator = new FileOrUrlValidator();
    // when valid URL
    // when invalid URL

    @Test
    public void validate_whenValidFile_doesNotThrowException() {
        fileOrUrlValidator.validate("--file", "pom.xml");
    }

    @Test
    public void validate_whenInvalidFile_ThrowsException() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("File provided for --file does not exist."));

        fileOrUrlValidator.validate("--file", "mop.lmx");
    }


    @Test
    public void validate_whenValidUrl_doesNotThrowException() {
        fileOrUrlValidator.validate("--file", "https://bitbucket.org/liox-ondemand/liox-content-sdk-java/raw/3d1ea2817e1db258006f7039c0242bcc1f2aa4a4/LICENSE.txt");
    }

    @Test
    public void validate_whenInvalidUrl_ThrowsException() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("URL provided for --file is invalid."));

        fileOrUrlValidator.validate("--file", "http://$%6//");
    }

}