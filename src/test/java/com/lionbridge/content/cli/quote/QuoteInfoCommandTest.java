package com.lionbridge.content.cli.quote;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;

public class QuoteInfoCommandTest {
    private QuoteInfoCommand quoteInfoCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        quoteInfoCommand = new QuoteInfoCommand();
        commander = new JCommander(quoteInfoCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse("--quote", "42");

        assertEquals(quoteInfoCommand.getQuoteId(), "42");
    }

    @Test
    public void requiresQuoteParameter() throws ParameterException {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --quote"));

        commander.parse();
    }
}