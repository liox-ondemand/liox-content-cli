package com.lionbridge.content.cli.quote;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.lionbridge.content.cli.quote.QuoteAuthorizeCommand;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class QuoteAuthorizeCommandTest {
    private QuoteAuthorizeCommand quoteAuthorizeCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        quoteAuthorizeCommand = new QuoteAuthorizeCommand();
        commander = new JCommander(quoteAuthorizeCommand);
    }

    @Test
    public void parsesParametersAndAssignsVslues() {
        commander.parse(
                "--quote=123",
                "--po-number=123456"
        );

        assertThat(quoteAuthorizeCommand.getQuote(), equalTo("123"));
        assertThat(quoteAuthorizeCommand.getPoNumber(), equalTo("123456"));
    }

    @Test
    public void requiresQuoteParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --quote"));

        commander.parse(
                "--po-number=42"
        );
    }

    @Test
    public void requiresPoNumberParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --po-number"));

        commander.parse(
                "--quote=42"
        );
    }
}