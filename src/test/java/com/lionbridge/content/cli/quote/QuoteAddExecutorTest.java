package com.lionbridge.content.cli.quote;

import com.lionbridge.content.cli.utilities.ProjectService;
import com.lionbridge.content.cli.utilities.TranslationOptionsService;
import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.models.Project;
import com.lionbridge.content.sdk.models.Quote;
import com.lionbridge.content.sdk.models.TranslationOptions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class QuoteAddExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private QuoteAddCommand quoteAddCommand;

    @Mock
    private TranslationOptionsService translationOptionsService;

    @Mock
    private TranslationOptions translationOptions;

    @Mock
    private List<Project> projects;

    @Mock
    private List<String> jobs;

    @Mock
    private List<String> endpoints;

    @Mock
    private Quote quote;

    @Mock
    private ProjectService projectService;

    @Before
    public void before() throws Exception {
        initMocks(this);

        when(projectService.createProjectsFromJobs(jobs)).thenReturn(projects);
        when(translationOptionsService.createQuoteAddTranslationOptions(quoteAddCommand)).thenReturn(translationOptions);
        when(quoteAddCommand.getJobs()).thenReturn(jobs);
        when(quoteAddCommand.getQuoteReadyEndPoints()).thenReturn(endpoints);
        when(contentApi.addQuote(translationOptions, projects, endpoints)).thenReturn(quote);
        when(quote.getQuoteId()).thenReturn(42);
    }

    @Test
    public void execute_withValidParameters_returnsQuoteId() {
        QuoteAddExecutor quoteAddExecutor = new QuoteAddExecutor(
                contentApi,
                quoteAddCommand,
                translationOptionsService,
                projectService
        );

        quoteAddExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("42\n"));
    }
}