package com.lionbridge.content.cli.quote;

import com.lionbridge.content.sdk.ContentAPI;
import com.lionbridge.content.sdk.models.QuoteAuthorization;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mock;

import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class QuoteAuthorizeExecutorTest {
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog().muteForSuccessfulTests();

    @Mock
    private ContentAPI contentApi;

    @Mock
    private QuoteAuthorizeCommand quoteAuthorizeCommand;

    @Before
    public void before() throws Exception {
        initMocks(this);

        QuoteAuthorization quoteAuthorization = new QuoteAuthorization();
        quoteAuthorization.setStatus("Authorized");

        when(quoteAuthorizeCommand.getQuote()).thenReturn("42");
        when(quoteAuthorizeCommand.getPoNumber()).thenReturn("123456");
        List<String> endpoints = null;
        when(quoteAuthorizeCommand.getQuotePaidEndPoints()).thenReturn(endpoints);

        when(contentApi.authorizeQuote("42", "123456", null)).thenReturn(quoteAuthorization);
    }

    @Test
    public void execute_withValidParameters_returnsQuoteAuthorization() {
        QuoteAuthorizeExecutor quoteAuthorizeExecutor = new QuoteAuthorizeExecutor(contentApi, quoteAuthorizeCommand);

        quoteAuthorizeExecutor.execute();

        assertThat(systemOutRule.getLog(), containsString("Success!\n"));
    }
}