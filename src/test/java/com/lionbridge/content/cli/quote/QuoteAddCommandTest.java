package com.lionbridge.content.cli.quote;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class QuoteAddCommandTest {
    private QuoteAddCommand quoteAddCommand;
    private JCommander commander;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        quoteAddCommand = new QuoteAddCommand();
        commander = new JCommander(quoteAddCommand);
    }

    @Test
    public void parsesParametersAndAssignsValues() {
        commander.parse(
                "--jobs=123,456",
                "--currency=USD",
                "--quote-ready-endpoint=foo@bar.com,http://foo.com/bar"
        );

        assertThat(quoteAddCommand.getJobs(), hasItems("123", "456"));
        assertThat(quoteAddCommand.getCurrency(), equalTo("USD"));
        assertThat(quoteAddCommand.getQuoteReadyEndPoints().size(), equalTo(2));
    }

    @Test
    public void requiresJobsParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --jobs"));

        commander.parse(
                "--currency=USD"
        );
    }

    @Test
    public void requiresCurrencyParameter() {
        thrown.expect(ParameterException.class);
        thrown.expectMessage(containsString("The following option is required: --currency"));

        commander.parse(
                "--jobs=42"
        );
    }
}